/**
  * This file is a part of Libcprime.
  * Library for saving activites and bookmarks, share file and more.
  * Copyright 2019 CuboCore Group
  *
  * This file is derived from QSingleApplication, originally written
  * as a part of Qt Solutions.
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, vsit http://www.gnu.org/licenses/.
  **/

#pragma once

#include <QCoreApplication>
#include <QLockFile>
#include <QLocalServer>
#include <QLocalSocket>

class CApplicationImpl: public QObject {
public:
	CApplicationImpl(const QString& id, QObject *parent = nullptr);

	~CApplicationImpl();

	bool isRunning();

	bool sendMessage(const QString& message, int timeout = 500);
	void disconnect();

	QString handleConnection();

	QLockFile *lockFile = nullptr;
	bool mLocked = false;

	QString mSocketName;
	QString mAppId;

	QLocalServer *mServer;
};
