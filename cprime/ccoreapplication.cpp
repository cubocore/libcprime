/**
  * This file is a part of Libcprime.
  * Library for saving activites and bookmarks, share file and more.
  * Copyright 2019 CuboCore Group
  *
  * This file is derived from QSingleApplication, originally written
  * as a part of Qt Solutions.
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, vsit http://www.gnu.org/licenses/.
  **/

#include "ccoreapplication.h"
#include "capplicationimpl.h"
#include <unistd.h>


namespace CPrime {
CCoreApplication::CCoreApplication(const QString& appId, int& argc, char **argv) : QCoreApplication(argc, argv)
{
	impl = new CApplicationImpl(appId);

	/** If we acquired the lock, then we can handle the server conenctions */
	if (impl->lockFile->isLocked())
	{
		/* Irrespective of what happens, we will try to connect newConnection to receiveConnection */
		QObject::connect(
			impl->mServer, &QLocalServer::newConnection, [ = ] () {
				QString msg = impl->handleConnection();
				if (msg.length())
				{
					emit messageReceived(msg);
				}
			}
			);
	}
}


CCoreApplication::~CCoreApplication()
{
	delete impl;
}


bool CCoreApplication::isRunning()
{
	return impl->isRunning();
}


bool CCoreApplication::sendMessage(const QString& message, int timeout)
{
	return impl->sendMessage(message, timeout);
}


QString CCoreApplication::id() const
{
	return impl->mAppId;
}
}
