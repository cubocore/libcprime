/**
  * This file is a part of Libcprime.
  * Library for saving activites and bookmarks, share file and more.
  * Copyright 2019 CuboCore Group
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, vsit http://www.gnu.org/licenses/.
  **/

#include <sys/types.h>
#include <unistd.h>
#include <pwd.h>

#include <QFile>
#include <QDir>
#include <QSettings>
#include <QUrl>
#include <QProcess>
#include <QStorageInfo>

#include "systemxdg.h"
#include "filefunc.h"
#include "variables.h"

using namespace CPrime;

/* We assume that the home partition is always mounted */
QString SystemXdg::homePartition = QStorageInfo(QDir::homePath()).rootPath();

QString SystemXdg::home()
{
	/* If the env variable HOME is set and its proper, good! */
	QString __home = QString::fromLocal8Bit(qgetenv("HOME"));

	if (access(__home.toLocal8Bit().data(), R_OK | X_OK) == 0)
	{
		return __home + "/";
	}

	/* Otherwise, we divine it from the user ID */
	struct passwd *pwd = getpwuid(getuid());

	return QString::fromLocal8Bit(pwd->pw_dir) + "/";
}


QString SystemXdg::xdgMimeApp(QString mimeType)
{
	/*
	  *
	  * We have the appsForMimeList. Now we need to filter some applications out as per user's choice and get the default value
	  * First check mimeapps.list/[Default Associations], then mimeapps.list/[Added Associations]. The entry corresponding to the mimetype in
	  * the first case and the first entry in the second case are the user defaults.
	  * If the mimetype is not listed, then check mimeinfo.cache
	  *
	  * Do the same for /usr/local/share/applications and /usr/share/applications
	  *
	  */

	QStringList files;

	files << QDir::home().filePath(".config/mimeapps.list");
	files << QDir::home().filePath(".local/share/applications/mimeapps.list");
	files << QDir::home().filePath(".local/share/applications/defaults.list");
	files << QDir::home().filePath(".local/share/applications/mimeinfo.cache");

	files << QDir::root().filePath("usr/local/share/applications/mimeapps.list");
	files << QDir::root().filePath("usr/local/share/applications/defaults.list");
	files << QDir::root().filePath("usr/local/share/applications/mimeinfo.cache");

	files << QDir::root().filePath("usr/share/applications/mimeapps.list");
	files << QDir::root().filePath("usr/share/applications/defaults.list");
	files << QDir::root().filePath("usr/share/applications/mimeinfo.cache");

	QString defaultValue;

	Q_FOREACH (const QString& file, files)
	{
		QSettings defaults(file, QSettings::NativeFormat);
		defaultValue = defaults.value(QString("Default Applications/%1").arg(mimeType)).toString();
		if (defaultValue.isEmpty())
		{
			defaultValue = defaults.value(QString("Added Associations/%1").arg(mimeType)).toString();
		}

		else
		{
			break;
		}

		if (defaultValue.isEmpty() or defaultValue.isNull())
		{
			continue;
		}

		else
		{
			break;
		}
	}

	return defaultValue;
}


QString SystemXdg::userDir(SystemXdg::XdgUserDir usrDir)
{
	switch (usrDir)
	{
	case SystemXdg::XdgUserDir::XDG_DATA_HOME:
		return QDir::home().filePath(".local/share/");

	case SystemXdg::XdgUserDir::XDG_CONFIG_HOME:
		return QDir::home().filePath(".config/");

	case SystemXdg::XdgUserDir::XDG_CACHE_HOME:
		return QDir::home().filePath(".cache/");
	}

	return QDir::homePath();
}


QStringList SystemXdg::systemDirs(SystemXdg::XdgSystemDirs sysDir)
{
	switch (sysDir)
	{
	case SystemXdg::XdgSystemDirs::XDG_DATA_DIRS:
		return QString(userDir(SystemXdg::XDG_DATA_HOME) + ":" + QDir::root().filePath("usr/local/share/") + ":" + QDir::root().filePath("usr/share/")).split(":", Qt::SkipEmptyParts);

	case SystemXdg::XdgSystemDirs::XDG_CONFIG_DIRS:
		return QString(userDir(SystemXdg::XDG_CONFIG_HOME) + ":" + QDir::root().filePath("etc/xdg/")).split(":", Qt::SkipEmptyParts);
	}

	return QStringList();
}


/**
  * SystemXdgMime
  **/

SystemXdgMime *SystemXdgMime::globalInstance = nullptr;

AppsList SystemXdgMime::appsForMimeType(QMimeType mimeType)
{
	AppsList      appsForMimeList;
	QStringList   mimeList = QStringList() << mimeType.name() << mimeType.allAncestors();
	QSet<QString> mimeSet(mimeList.begin(), mimeList.end());

	Q_FOREACH (const DesktopFile& app, appsList)
	{
		QStringList   mmTypes = app.mimeTypes();
		QSet<QString> temp(mmTypes.begin(), mmTypes.end());
		QSet<QString> intersected = temp.intersect(mimeSet);
		if (intersected.count())
		{
			if ((app.type() == DesktopFile::Application) and app.visible())
			{
				appsForMimeList << app;
			}
		}
	}

	QString defaultName = SystemXdg::xdgMimeApp(mimeType.name());

	for (int i = 0; i < appsForMimeList.count(); i++)
	{
		if (appsForMimeList.value(i).desktopName() == CPrime::FileUtils::baseName(defaultName))
		{
			appsForMimeList.move(i, 0);
			break;
		}
	}

	return appsForMimeList;
}


QStringList SystemXdgMime::mimeTypesForApp(QString desktopName)
{
	QStringList mimeList;

	if (not desktopName.endsWith(".desktop"))
	{
		desktopName += ".desktop";
	}

	Q_FOREACH (const QString& appDir, appsDirs)
	{
		if (QFile::exists(appDir + desktopName))
		{
			mimeList << DesktopFile(appDir + desktopName).mimeTypes();
			break;
		}
	}

	return mimeList;
}


AppsList SystemXdgMime::allDesktops()
{
	return appsList;
}


DesktopFile SystemXdgMime::application(QString exec)
{
	AppsList list;

	Q_FOREACH (const DesktopFile& app, appsList)
	{
		if (app.command().contains(exec, Qt::CaseSensitive))
		{
			list << app;
		}

		else if (app.name().compare(exec, Qt::CaseInsensitive) == 0)
		{
			list << app;
		}
	}

	if (not list.count())
	{
		return DesktopFile();
	}

	int rank = -1, index = -1;

	for (int i = 0; i < list.count(); i++)
	{
		if (rank < list.value(i).rank())
		{
			rank  = list.value(i).rank();
			index = i;
		}
	}

	/* Desktop file with the highest rank will be used always */
	return list.at(index);
}


QString SystemXdgMime::desktopPathForName(QString desktopName)
{
	if (not desktopName.endsWith(".desktop"))
	{
		desktopName += ".desktop";
	}

	if (CPrime::FileUtils::exists(desktopName))
	{
		return desktopName;
	}

	Q_FOREACH (const QString& appDirStr, appsDirs)
	{
		if (CPrime::FileUtils::exists(appDirStr + "/" + desktopName))
		{
			return appDirStr + "/" + desktopName;
		}
	}

	return QString();
}


DesktopFile SystemXdgMime::desktopForName(QString desktopName)
{
	if (not desktopName.endsWith(".desktop"))
	{
		desktopName += ".desktop";
	}

	if (CPrime::FileUtils::exists(desktopName))
	{
		return DesktopFile(desktopName);
	}

	QString desktopPath;

	Q_FOREACH (const QString& appDirStr, appsDirs)
	{
		if (CPrime::FileUtils::exists(appDirStr + "/" + desktopName))
		{
			desktopPath = appDirStr + "/" + desktopName;
			break;
		}
	}

	return DesktopFile(desktopPath);
}


void SystemXdgMime::parseDesktops()
{
	appsList.clear();
	Q_FOREACH (const QString& appDirStr, appsDirs)
	{
		QDir appDir(appDirStr);
		Q_FOREACH (const QFileInfo& desktop, appDir.entryInfoList(QStringList() << "*.desktop", QDir::Files))
		{
			appsList << DesktopFile(desktop.absoluteFilePath());
		}
	}
}


SystemXdgMime *SystemXdgMime::instance()
{
	if (SystemXdgMime::globalInstance)
	{
		return globalInstance;
	}

	SystemXdgMime::globalInstance = new SystemXdgMime();
	globalInstance->parseDesktops();

	return SystemXdgMime::globalInstance;
}


void SystemXdgMime::setApplicationAsDefault(QString appFileName, QString mimetype)
{
	if (QProcess::execute("xdg-mime", QStringList() << "default" << appFileName << mimetype))
	{
		qDebug() << "Error while setting" << appFileName << "as the default handler for" << mimetype;
	}
}


SystemXdgMime::SystemXdgMime()
{
	appsDirs << QDir::home().filePath(".local/share/applications/");
	appsDirs << QDir::root().filePath("usr/local/share/applications/") << QDir::root().filePath("usr/share/applications/");
	appsDirs << QDir::root().filePath("usr/share/applications/kde4/") << QDir::root().filePath("usr/share/gnome/applications/");
}


DesktopFile SystemXdgMime::xdgDefaultApp(QMimeType mimeType)
{
	return appsForMimeType(mimeType).value(0);
}


void SystemDefaultApps::setDefaultApp(CPrime::DefaultAppCategory category, const QString& desktopFileName)
{
	qDebug() << "Check if the config folder exist";
	CPrime::FileUtils::setupFolder(CPrime::FolderSetup::ConfigFolder);

	QString categoryName = "None";

	if (category == CPrime::FileManager)
	{
		categoryName = "FileManager";
	}
	else if (category == CPrime::MetadataViewer)
	{
		categoryName = "MetadataViewer";
	}
	else if (category == CPrime::SearchApp)
	{
		categoryName = "SearchApp";
	}
	else if (category == CPrime::ImageEditor)
	{
		categoryName = "ImageEditor";
	}
	else if (category == CPrime::Terminal)
	{
		categoryName = "Terminal";
	}
	else if (category == CPrime::BatchRenamer)
	{
		categoryName = "BatchRenamer";
	}

	QSettings defaultSettings(CPrime::Variables::CC_DefaultAppListFilePath(), QSettings::NativeFormat);

	defaultSettings.setValue(QString("Default_Applications/%1").arg(categoryName), desktopFileName);
}


QString SystemDefaultApps::getDefaultApp(CPrime::DefaultAppCategory category)
{
	QString categoryName = "None";

	if (category == CPrime::FileManager)
	{
		categoryName = "FileManager";
	}
	else if (category == CPrime::MetadataViewer)
	{
		categoryName = "MetadataViewer";
	}
	else if (category == CPrime::SearchApp)
	{
		categoryName = "SearchApp";
	}
	else if (category == CPrime::ImageEditor)
	{
		categoryName = "ImageEditor";
	}
	else if (category == CPrime::Terminal)
	{
		categoryName = "Terminal";
	}
	else if (category == CPrime::BatchRenamer)
	{
		categoryName = "BatchRenamer";
	}

	QString   defaultValue = "";
	QSettings defaultSettings(CPrime::Variables::CC_DefaultAppListFilePath(), QSettings::NativeFormat);

	defaultValue = defaultSettings.value(QString("Default_Applications/%1").arg(categoryName)).toString();

	if (defaultValue.isEmpty())
	{
		return "";
	}

	return SystemXdgMime::instance()->desktopPathForName(defaultValue);
}
