/**
  * This file is a part of Libcprime.
  * Library for saving activites and bookmarks, share file and more.
  * Copyright 2019 CuboCore Group
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, vsit http://www.gnu.org/licenses/.
  **/

#pragma once

#include <QMimeType>

#include <cprime/desktopfile.h>
#include <cprime/cenums.h>
#include <cprime/libcprime_global.h>

namespace CPrime {
class LIBCPRIMESHARED_EXPORT SystemXdg {
public:
	enum XdgUserDir
	{
		XDG_DATA_HOME = 0x02544878,
		XDG_CONFIG_HOME,
		XDG_CACHE_HOME
	};

	enum XdgSystemDirs
	{
		XDG_DATA_DIRS = 0x196BB115,
		XDG_CONFIG_DIRS
	};

	static QString home();
	static QString xdgMimeApp(QString);
	static QString userDir(SystemXdg::XdgUserDir);
	static QStringList systemDirs(SystemXdg::XdgSystemDirs);

	static QString trashLocation(QString path);
	static QString homeTrashLocation();

	static QString homePartition;
};

typedef QList<DesktopFile> AppsList;

class LIBCPRIMESHARED_EXPORT SystemXdgMime {
public:
	static SystemXdgMime *instance();

	// Get a list of applications for a mime type given
	AppsList appsForMimeType(QMimeType);

	// Get a list of mimetypes an application handles, given the desktop name
	QStringList mimeTypesForApp(QString);

	// List all the applications
	AppsList allDesktops();

	// Get the consolidated/unified application file for a desktop name
	DesktopFile application(QString);

	// Get the best desktop file path for a desktop name
	QString desktopPathForName(QString);

	// Get the best desktop file path for a desktop name
	DesktopFile desktopForName(QString);

	// Add one new application location
	void addAppsLocations(QString);

	// Add multiple applications locations
	void addAppsLocations(QStringList);

	// Parse all desktops
	void parseDesktops();

	static void setApplicationAsDefault(QString, QString);

	DesktopFile xdgDefaultApp(QMimeType);

private:
	SystemXdgMime();

	static SystemXdgMime *globalInstance;

	QStringList appsDirs;
	AppsList appsList;
};

class LIBCPRIMESHARED_EXPORT SystemDefaultApps {
public:
	static void setDefaultApp(CPrime::DefaultAppCategory category, const QString& desktopFileName);
	static QString getDefaultApp(CPrime::DefaultAppCategory category);
};
}
