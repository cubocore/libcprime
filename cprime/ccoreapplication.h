/**
  * This file is a part of Libcprime.
  * Library for saving activites and bookmarks, share file and more.
  * Copyright 2019 CuboCore Group
  *
  * This file is derived from QSingleApplication, originally written
  * as a part of Qt Solutions.
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, vsit http://www.gnu.org/licenses/.
  **/

#pragma once

#include <QCoreApplication>
#include <QLockFile>
#include <QLocalServer>
#include <QLocalSocket>

namespace CPrime {
class CCoreApplication;
}

class CApplicationImpl;

#if defined(qApp)
#undef qApp
#endif

#define qApp    (static_cast<CPrime::CCoreApplication *>(QCoreApplication::instance()))

class CPrime::CCoreApplication : public QCoreApplication {
	Q_OBJECT

public:
	CCoreApplication(const QString& id, int& argc, char **argv);

	~CCoreApplication();

	bool isRunning();
	QString id() const;

public Q_SLOTS:
	bool sendMessage(const QString& message, int timeout = 500);

Q_SIGNALS:
	void messageReceived(const QString& message);

private:
	CApplicationImpl *impl;
};
