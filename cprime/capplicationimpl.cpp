/**
  * This file is a part of Libcprime.
  * Library for saving activites and bookmarks, share file and more.
  * Copyright 2019 CuboCore Group
  *
  * This file is derived from QSingleApplication, originally written
  * as a part of Qt Solutions.
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, vsit http://www.gnu.org/licenses/.
  **/

#include "capplicationimpl.h"
#include <unistd.h>

static inline QString getSocketName(QString appId)
{
	/** Get the env-var XDG_RUNTIME_DIR */
	QString sockName(qgetenv("XDG_RUNTIME_DIR"));

	/** The env-var was not set. We will use /tmp/ */
	if (not sockName.length())
	{
		sockName = "/tmp/";
	}

	if (not sockName.endsWith("/"))
	{
		sockName += "/";
	}

	/** Append a random number */
	sockName += QString("%1-Scoket-%2").arg(appId).arg(getuid());

	return sockName;
}


CApplicationImpl::CApplicationImpl(const QString& appId, QObject *parent) : QObject( parent )
{
	mServer = nullptr;

	/* App ID */
	mAppId = appId;

	/** Get the env-var XDG_RUNTIME_DIR */
	mSocketName = getSocketName(mAppId);

	/* Lock File */
	lockFile = new QLockFile(mSocketName + ".lock");

	/* Try to lock the @lockFile, if it fails, then we're not the first instance */
	if (lockFile->tryLock())
	{
		/* Local Server for communication */
		mServer = new QLocalServer(this);

		/* Start the server */
		bool res = mServer->listen(mSocketName);

		/* @res can't be false at the moment, because we're the first instance. */
		/* The only reason why @res is false, the socket file exists from a previous */
		/* crash. So delete it and try again. */
		if (not res && (mServer->serverError() == QAbstractSocket::AddressInUseError))
		{
			QLocalServer::removeServer(mSocketName);
			res = mServer->listen(mSocketName);

			if (!res)
			{
				qWarning("CPrime::CApplicationImpl: listen on local socket failed, %s", qPrintable(mServer->errorString()));
			}
		}
	}
}


CApplicationImpl::~CApplicationImpl()
{
	disconnect();

	delete lockFile;
}


bool CApplicationImpl::isRunning()
{
	/* If we have the lock, we're the server */
	/* In other words, if we're not there, there is no server */
	if (lockFile->isLocked())
	{
		return false;
	}

	/* If we cannot get the lock then the server is running elsewhere */
	if (not lockFile->tryLock())
	{
		return true;
	}

	/* Be default, we'll assume that the server is running elsewhere */
	return true;
}


bool CApplicationImpl::sendMessage(const QString& message, int timeout)
{
	if (not isRunning())
	{
		return false;
	}

	/* Preparing socket */
	QLocalSocket socket(this);

	/* Connecting to server */
	socket.connectToServer(mSocketName);

	/* Wait for ACK (500 ms) */
	if (not socket.waitForConnected(timeout))
	{
		return false;
	}

	/* Send the message to the server */
	socket.write(message.toUtf8());

	/** Should finish writing in 500 ms */
	return socket.waitForBytesWritten(timeout);
}


void CApplicationImpl::disconnect()
{
	if (mServer)
	{
		mServer->close();
	}

	lockFile->unlock();
}


QString CApplicationImpl::handleConnection()
{
	/* Preparing socket */
	QLocalSocket *socket = mServer->nextPendingConnection();

	if (socket == nullptr)
	{
		return QString();
	}

	socket->waitForReadyRead(2000);
	QByteArray msg = socket->readAll();

	/** Close the connection */
	socket->close();

	return QString(msg);
}
