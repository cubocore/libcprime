/**
  * This file is a part of Libcprime.
  * Library for saving activites and bookmarks, share file and more.
  * Copyright 2019 CuboCore Group
  *
  * This file is derived from QSingleApplication, originally written
  * as a part of Qt Solutions.
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, vsit http://www.gnu.org/licenses/.
  **/

#include <unistd.h>

#include "capplication.h"
#include "capplicationimpl.h"
#include <QWidget>

static inline QString getSocketName(QString appId)
{
	/** Get the env-var XDG_RUNTIME_DIR */
	QString sockName(qgetenv("XDG_RUNTIME_DIR"));

	/** The env-var was not set. We will use /tmp/ */
	if (not sockName.length())
	{
		sockName = "/tmp/";
	}

	if (not sockName.endsWith("/"))
	{
		sockName += "/";
	}

	/** Append a random number */
	sockName += QString("%1-Scoket-%2").arg(appId).arg(getuid());

	return sockName;
}


CPrime::CApplication::CApplication(const QString& appId, int& argc, char **argv) : QApplication(argc, argv)
{
	actWin = nullptr;

	impl = new CApplicationImpl(appId);

	/** If we acquired the lock, then we can handle the server conenctions */
	if (impl->lockFile->isLocked())
	{
		/* Irrespective of what happens, we will try to connect newConnection to receiveConnection */
		QObject::connect(
			impl->mServer, &QLocalServer::newConnection, [ = ] () {
			QString msg = impl->handleConnection();
			if (msg.length())
			{
				emit messageReceived(msg);
			}
		}
			);
	}
}


CPrime::CApplication::~CApplication()
{
	delete impl;
}


bool CPrime::CApplication::isRunning()
{
	return impl->isRunning();
}


QString CPrime::CApplication::id() const
{
	return impl->mAppId;
}


bool CPrime::CApplication::sendMessage(const QString& message, int timeout)
{
	return impl->sendMessage(message, timeout);
}


void CPrime::CApplication::setActivationWindow(QWidget *aw, bool activateOnMessage)
{
	/* Activation makes sense only when the server is active */
	if (impl->mServer)
	{
		actWin = aw;
		if (activateOnMessage)
		{
			QObject::connect(this, &CPrime::CApplication::messageReceived, this, &CPrime::CApplication::activateWindow);
		}

		else
		{
			QObject::disconnect(this, &CPrime::CApplication::messageReceived, this, &CPrime::CApplication::activateWindow);
		}
	}
}


QWidget *CPrime::CApplication::activationWindow() const
{
	return actWin;
}


void CPrime::CApplication::activateWindow()
{
	if (actWin)
	{
		actWin->show();
		actWin->setWindowState(actWin->windowState() & ~Qt::WindowMinimized);
		actWin->raise();
		actWin->activateWindow();
	}
}
