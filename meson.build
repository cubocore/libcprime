project(
	'libcprime',
	'cpp',
	version: '5.0.0',
	license: 'GPLv3',
	meson_version: '>=0.59.0',
	default_options: [
		'cpp_std=c++17',
		'c_std=c11',
		'warning_level=2',
		'werror=false',
	],
)

add_project_arguments( '-DPROJECT_VERSION="@0@"'.format( meson.project_version() ), language : 'cpp' )
add_project_link_arguments( ['-rdynamic','-fPIC'], language:'cpp' )

GlobalInc = include_directories( [ '.', 'cprime' ] )

Qt = import( 'qt6' )

CoreDeps    = dependency( 'qt6', modules: [ 'Core', 'DBus', 'Network' ] )
GuiDeps     = dependency( 'qt6', modules: [ 'Core', 'Gui', 'DBus', 'Network' ] )
WidgetsDeps = dependency( 'qt6', modules: [ 'Core', 'Gui', 'Widgets', 'DBus', 'Network' ] )

CoreHeaders = [
	'cprime/activitesmanage.h',
	'cprime/cenums.h',
	'cprime/desktopfile.h',
	'cprime/filefunc.h',
	'cprime/messageengine.h',
	'cprime/sortfunc.h',
	'cprime/systemxdg.h',
	'cprime/variables.h',
	'cprime/ccoreapplication.h',
	'cprime/libcprime_global.h'
]

CoreSources = [
	'cprime/activitesmanage.cpp',
	'cprime/capplicationimpl.cpp',
	'cprime/ccoreapplication.cpp',
	'cprime/desktopfile.cpp',
	'cprime/filefunc.cpp',
	'cprime/messageengine.cpp',
	'cprime/sortfunc.cpp',
	'cprime/systemxdg.cpp',
	'cprime/variables.cpp'
]

CoreMocs = Qt.compile_moc(
	headers : CoreHeaders,
	dependencies: CoreDeps
)
includes = [ 'cprime' ]

cprimecore = shared_library(
        'cprime-core', [ CoreSources, CoreMocs ],
	version: meson.project_version(),
	include_directories: [ includes ],
	dependencies: CoreDeps,
	install: true,
	install_dir: join_paths( get_option( 'libdir' ) )
)

GuiHeaders = [
	'cprime/cguiapplication.h'
]

GuiSources = [
	'cprime/cguiapplication.cpp'
]

GuiMocs = Qt.compile_moc(
 	headers : GuiHeaders,
 	dependencies: GuiDeps
)

cprimegui = shared_library(
    'cprime-gui', [ GuiSources, GuiMocs ],
	version: meson.project_version(),
	include_directories: [ includes ],
    dependencies: [ GuiDeps ],
	link_with: cprimecore,
    install: true,
    install_dir: get_option( 'libdir' )
)

WidgetsHeaders = [
	'cprime/appopenfunc.h',
	'cprime/cprime.h',
	'cprime/pinmanage.h',
	'cprime/themefunc.h',
	'cprime/trashmanager.h',
	'cprime/capplication.h',
	'cprime/applicationdialog.h',
	'cprime/cplugininterface.h',
	'cprime/ioprocesses.h',
	'cprime/pinit.h',
	'cprime/shareit.h'
]

WidgetsSources = [
	'cprime/capplication.cpp',
	'cprime/applicationdialog.cpp',
	'cprime/appopenfunc.cpp',
	'cprime/ioprocesses.cpp',
	'cprime/pinit.cpp',
	'cprime/pinmanage.cpp',
	'cprime/shareit.cpp',
	'cprime/themefunc.cpp',
	'cprime/trashmanager.cpp',
	'cprime/cplugininterface.cpp'
]

WidgetsUI = [
	'cprime/pinit.ui',
	'cprime/shareit.ui'
]

WidgetsMocs = Qt.compile_moc(
 	headers : WidgetsHeaders,
	dependencies: WidgetsDeps
)

WidgetsUIs = Qt.compile_ui(
 	sources : WidgetsUI
)

cprimewidgets = shared_library(
    'cprime-widgets', [ WidgetsSources, WidgetsMocs, WidgetsUIs ],
	version: meson.project_version(),
	include_directories: [ includes ],
    dependencies: [ WidgetsDeps ],
	link_with: cprimecore,
    install: true,
    install_dir: join_paths( get_option( 'libdir' ) )
)

# Install the headers
install_headers(
	CoreHeaders, GuiHeaders, WidgetsHeaders,
	subdir: 'cprime'
)

## PkgConfig Section
pkgconfig = import( 'pkgconfig' )
pkgconfig.generate(
	cprimecore,
        name: 'cprime-core',
	version: meson.project_version(),
        filebase: 'cprime-core',
	description: 'Library for saving activites and bookmarks, share file and more. (Core module)',
)

pkgconfig.generate(
	cprimegui,
        name: 'cprime-gui',
	version: meson.project_version(),
        filebase: 'cprime-gui',
	description: 'Library for saving activites and bookmarks, share file and more. (Gui module)',
        requires: 'cprime-core'
)

pkgconfig.generate(
	cprimewidgets,
        name: 'cprime-widgets',
	version: meson.project_version(),
        filebase: 'cprime-widgets',
	description: 'Library for saving activites and bookmarks, share file and more. (Widgets module)',
        requires: 'cprime-core'
)
